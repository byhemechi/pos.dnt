const path = require("path");
const fs = require("fs");

module.exports = (req, res, next) => {
	var npath = path.join("./api", req.url.replace(/(^.*?\/.*?)\/.*/, "$1"));
	if(fs.existsSync(npath.replace(/\.js$/, "") + ".js")) {
		var fnpath = req.url.split("");
		fnpath.splice(0, npath.length - 3);
		fnpath = fnpath.join("");
		req.url = fnpath;
		const sender = require("./" + npath.replace(/\.js$/, ""));
		if(sender.constructor == Function) {
			res.json(sender(req, next))
		} else {
			res.json(sender);
		}
	} else {
		next()
	}
}