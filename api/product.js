const products = require("./products")
module.exports = (req, next) => {
	console.log(products[req.url.replace(/^\//,"")])
	if(products[req.url.replace(/^\//,"")]) {
		return products[req.url.replace(/^\//,"")]
	} else {
		return {
			error: 404,
			message: "Product not found"
		}
	}
}