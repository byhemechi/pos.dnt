module.exports = {
	cap: {
		"name": "Cappuccino",
		"price": 2.5,
		"img": "http://pngimg.com/uploads/cappuccino/cappuccino_PNG73.png"
	},
	black: {
		name: "Long Black",
		price: 2,
		"img": "https://upload.wikimedia.org/wikipedia/commons/4/45/A_small_cup_of_coffee.JPG"
	},
	flat: {
		name: "Flat white",
		price: 2,
		"img": "https://www.bbcgoodfood.com/sites/default/files/styles/recipe/public/recipe/recipe-image/2018/03/flat-white.jpg?itok=tQRxJOLV"
	},
	anzac: {
		name: "ANZAC Biscuit",
		price: 2,
		"img": "https://www.bhg.com.au/media/9838/170310-gluten-free-anzac-biscuits.jpg"
	},
	banana: {
		name: "Banana Bread",
		price: 0.5,
		"img": "https://www.cookingclassy.com/wp-content/uploads/2012/04/banana-bread-3-500x500.jpg"
	},
	cake: {
		name: "Slice of cake",
		price: 3.5,
		"img": "https://d3dz4rogqkqh6r.cloudfront.net/uploads/files/2016/09/yimg_NEhDaS.jpg"
	}
}