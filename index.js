const express = require("express");
const app = express();
const path = require("path");
const https = require("https");
const fs = require("fs");

app.set("view engine", "pug");

app.set("views", path.join(__dirname, "views"))

app.use("/api", require("./api"));

app.use("/static", express.static(__dirname + "/static"))

app.get("/", function(req, res) {
	res.render("home");
})

app.get("/checkout", (req, res) => res.render("checkout"))

app.listen(process.env.PORT || 8080)