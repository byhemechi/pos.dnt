/**
 * Get the data from an pi path
 * @param {string} path The path to get
 */
const get = async (path) => await(await fetch("/api/" + path)).json();

export default {get};