import api from "./api.js";

/**
 * Adds an item to cart
 * @param {string} item The product id of the item to add
 * @param {number} count The number of items to add
 */
var products = [];
function cleanup() {
    const cart = JSON.parse(sessionStorage.getItem("cart")) || [];
    var remove = []
    cart.forEach((el, i) => {
        if(el.count <= 0) {
            remove.push(i)
        }
    });
    remove.forEach(el => {
        cart.splice(el, 1)
    });
    sessionStorage.setItem("cart", JSON.stringify(cart));
}

function add(item, count) {
    const cart = JSON.parse(sessionStorage.getItem("cart")) || [];

    // Check if the item is already in the cart
    const id = cart.findIndex(function(i) {
        return i.id == item;
    });

    if(id > -1) cart[id].count += count;
    else cart.push({id: item, count: 1})
    sessionStorage.setItem("cart", JSON.stringify(cart))
}

function remove(item, count) {
    const cart = JSON.parse(sessionStorage.getItem("cart")) || [];

    // Check if the item is already in the cart
    const id = cart.findIndex(function(i) {
        return i.id == item;
    });

    if(id > -1) cart[id].count -= count;
    else cart.push({id: item, count: 1})

    
    sessionStorage.setItem("cart", JSON.stringify(cart))
    cleanup();
}

function get() {
    return JSON.parse(sessionStorage.getItem("cart")) || []
}

async function render() {
    const el = document.querySelector("#cart");
    el.innerHTML = "";
    var total = 0;
    products = await api.get("products")
    get().forEach(i => {
        const newel = document.createElement("div");
        newel.textContent = `${i.count} × ${products[i.id].name}`;

        total += products[i.id].price * i.count;

        const subbtn = document.createElement("button");
        subbtn.textContent = "-";
        subbtn.onclick = e => {
            e.preventDefault();
            remove(i.id, 1);
            render();
        }
        el.appendChild(newel);
        const removebtn = document.createElement("button");
        removebtn.textContent = "x";
        removebtn.onclick = e => {
            e.preventDefault();
            remove(i.id, i.count);
            render();
        }
        newel.appendChild(removebtn)
        newel.appendChild(subbtn)
        const price = document.createElement("div");
        price.textContent = `$${products[i.id].price.toFixed(2)}`
        price.className = "price"
        newel.appendChild(price);
        el.appendChild(newel);
    })
    const checkbtn = document.createElement("a");
    checkbtn.className = get().length ? "" : "disabled"
    checkbtn.href = "/checkout";
    checkbtn.textContent = total > 0 ? `$${total.toFixed(2)}` : "Cart is empty";
    
    if(typeof(PaymentRequest) != "undefined") {
        const dsp = [];
        PaymentRequest = PaymentRequest || void(0)
        get().forEach(i => {
            dsp.push({
                label: `${i.count} × ${products[i.id].name}`,
                id: i.id,
                amount: {currency: "AUD", value: (products[i.id].price * i.count).toFixed(2)},
                count: i.count
            })
        })
        checkbtn.onclick = async e => {
            const pr = new PaymentRequest([{supportedMethods: "basic-card"}], {
                total: {label: "Total", amount: {currency: "AUD", "value": total.toFixed(2)}},
                displayItems: dsp
            });
            
            e.preventDefault();
            if(total > 0) {
                const show = await pr.show();
                setTimeout(() => show.complete(), 1000 + Math.random() * 1000)
            }
        }
    }
    const clearbtn = document.createElement("a");
    clearbtn.textContent = "Clear Cart";
    clearbtn.href = "#"
    clearbtn.onclick = e => {
        e.preventDefault();
        sessionStorage.setItem("cart", "[]");
        render()
    };
    clearbtn.className = 'white'
    el.appendChild(checkbtn)
    if(total > 0) el.appendChild(clearbtn)
}

export default {add, remove, get, render};