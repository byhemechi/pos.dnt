import cart from "./cart.js";
import api from "./api.js";

(async () => {
    const products = await api.get("products");
    var total = 0;

    cart.get().forEach(i => {
        const el = document.createElement("li");
        el.textContent = `${i.count} × ${products[i.id].name}`;
        const price = document.createElement("div");
        price.className = "price";
        price.textContent = "$" + (i.count * products[i.id].price).toFixed(2);
        el.appendChild(price)
        total += i.count * products[i.id].price;
        document.querySelector("#items").appendChild(el)
    })
    {
        const el = document.createElement("li");
        el.textContent = "Total";
        const price = document.createElement("div");
        price.className = "price";
        price.textContent = "$" + total.toFixed(2);
        el.appendChild(price)
        document.querySelector("#items").appendChild(el)
    }
})()