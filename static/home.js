import api from "./api.js";
import cart from "./cart.js";

(async () => {
	const body = document.querySelector("#menu");
	const products = await api.get("products");
	for(let i in products) {
		const newel = document.createElement("a");
		newel.href = `/api/product/${i}`;
		newel.className = "item"
		// newel.textContent = `${products[i].name}  ($${products[i].price.toFixed(2)})`;
		const img = document.createElement("div");
		img.className = "img"
		img.style.backgroundImage = `url(${products[i].img})`;
		const details = document.createElement("div");
		details.className = "details";
		details.innerHTML = `<div>${products[i].name}</div><div>($${products[i].price.toFixed(2)})</div>`;
		newel.appendChild(img)
		newel.appendChild(details)

		newel.onclick = (e) => {
			e.preventDefault();
			cart.add(i, 1);
			cart.render();
		}

		body.appendChild(newel)
	}

	cart.render();
})();